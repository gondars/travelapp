import {Component, Vue, Watch} from 'vue-property-decorator';

@Component({
    props: {
        placeholder: Boolean,
        readonly: Boolean,
        required: Boolean,
        autofocus: Boolean,
        type: String,
        min: Number,
        value: String
    }
})
export default class TextField extends Vue {
    public value: any;

    @Watch('value')
    public valueChanged() {
        this.$emit('valueChanged', this.value);
    }
}
