import {Component, Vue} from 'vue-property-decorator'
import TextField from '@/components/TextField/TextField.vue'
import axios from 'axios';

@Component({
    components: {
        TextField
    },
})
export default class Home extends Vue{
    private inputText: string | undefined;
    private performers: any;

    public data() {
        return {
            inputText: '',
            performers: []
        }
    }
    public onSearchClick() {
        axios.get(`https://api.seatgeek.com/2/performers?q=${this.inputText}&client_id=MjEyMDE4MTF8MTU5MTE4OTYzOS40OQ`)
            .then((response) => {
                this.performers = response.data.performers;
            });
    }
}
